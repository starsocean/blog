json.array!(@commnets) do |commnet|
  json.extract! commnet, :id, :post_id, :body
  json.url commnet_url(commnet, format: :json)
end
