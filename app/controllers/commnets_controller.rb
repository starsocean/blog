class CommnetsController < ApplicationController
  before_action :set_commnet, only: [:show, :edit, :update, :destroy]

  # GET /commnets
  # GET /commnets.json
  def index
    @commnets = Commnet.all
  end

  # GET /commnets/1
  # GET /commnets/1.json
  def show
  end

  # GET /commnets/new
  def new
    @commnet = Commnet.new
  end

  # GET /commnets/1/edit
  def edit
  end

  # POST /commnets
  # POST /commnets.json
  def create
    @commnet = Commnet.new(commnet_params)

    respond_to do |format|
      if @commnet.save
        format.html { redirect_to @commnet, notice: 'Commnet was successfully created.' }
        format.json { render :show, status: :created, location: @commnet }
      else
        format.html { render :new }
        format.json { render json: @commnet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /commnets/1
  # PATCH/PUT /commnets/1.json
  def update
    respond_to do |format|
      if @commnet.update(commnet_params)
        format.html { redirect_to @commnet, notice: 'Commnet was successfully updated.' }
        format.json { render :show, status: :ok, location: @commnet }
      else
        format.html { render :edit }
        format.json { render json: @commnet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /commnets/1
  # DELETE /commnets/1.json
  def destroy
    @commnet.destroy
    respond_to do |format|
      format.html { redirect_to commnets_url, notice: 'Commnet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_commnet
      @commnet = Commnet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def commnet_params
      params.require(:commnet).permit(:post_id, :body)
    end
end
