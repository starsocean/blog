require 'test_helper'

class CommnetsControllerTest < ActionController::TestCase
  setup do
    @commnet = commnets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:commnets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create commnet" do
    assert_difference('Commnet.count') do
      post :create, commnet: { body: @commnet.body, post_id: @commnet.post_id }
    end

    assert_redirected_to commnet_path(assigns(:commnet))
  end

  test "should show commnet" do
    get :show, id: @commnet
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @commnet
    assert_response :success
  end

  test "should update commnet" do
    patch :update, id: @commnet, commnet: { body: @commnet.body, post_id: @commnet.post_id }
    assert_redirected_to commnet_path(assigns(:commnet))
  end

  test "should destroy commnet" do
    assert_difference('Commnet.count', -1) do
      delete :destroy, id: @commnet
    end

    assert_redirected_to commnets_path
  end
end
